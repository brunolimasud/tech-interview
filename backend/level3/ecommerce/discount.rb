class Discount
  attr_accessor :article_id, :type, :value

  def initialize article_id, type, value
  	@article_id = article_id 
  	@type = type
  	@value = value
  end

  # calculate the final value with the specified discount
  def calculate_price article_value
  	amount? ? article_value - value : article_value * (1-value/100.0)
  end

  def amount?
  	type == 'amount'
  end

  def percentage?
  	type == 'percentage'
  end
end