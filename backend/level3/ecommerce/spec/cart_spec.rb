require_relative '../cart.rb'
require_relative '../article.rb'
require_relative '../delivery_fee.rb'
require_relative '../discount.rb'


describe Cart do
  articles = [Article.new(1, "banana", 30), Article.new(2, "grape", 50)]
  items = [{"article_id":1, "quantity": 20}, {"article_id": 2, "quantity": 10}]
  cart = Cart.new(1, items)

  delivery_fees = [DeliveryFee.new(800..2000, 400)]
  discounts = [Discount.new(1, "amount", 5), Discount.new(2, "percentage", 10)]

  it "Verify value total" do
    expect(cart.total(articles)).to eq(1100)
  end

  it "Verify value with delivery fee" do
    expect(cart.total(articles, delivery_fees)).to eq(1500)
  end

  it "Verify value with discounts" do
    expect(cart.total(articles, delivery_fees, discounts)).to eq(1350)
  end
end