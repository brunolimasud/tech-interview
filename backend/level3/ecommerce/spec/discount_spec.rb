require_relative '../discount.rb'

describe Discount do
  it "Verify discount amount" do
  	discount = Discount.new(1, "amount", 10)

  	expect(discount.amount?).to be true
    expect(discount.calculate_price(100)).to eq(90)
  end

  it "Verify discount percentage" do
  	discount = Discount.new(1, "percentage", 20)

  	expect(discount.percentage?).to be true
    expect(discount.calculate_price(100)).to eq(80)
  end
end