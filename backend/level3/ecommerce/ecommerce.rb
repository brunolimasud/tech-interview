require 'json'
require 'byebug'

require './article'
require './cart'
require './delivery_fee'
require './discount'

class Ecommerce
  attr_accessor :articles, :carts

  def initialize articles, carts, delivery_fees, discounts
  	@articles = articles
  	@carts = carts
  	@delivery_fees = delivery_fees
  	@discounts = discounts
  end

  # load data of the JSON file
  def self.load_data
  	data_file = File.read('data.json')
		data_hash = JSON.parse(data_file)

		articles = []
		carts = []
		delivery_fees = []
		discounts = []

		data_hash['articles'].each do |article_hash|
			articles << Article.new(article_hash['id'], article_hash['name'], article_hash['price'])
		end

		data_hash['carts'].each do |cart_hash|
			carts << Cart.new(cart_hash['id'], cart_hash['items'])
		end

		data_hash['delivery_fees'].each do |delivery_fee_hash|
			range = delivery_fee_hash['eligible_transaction_volume']

			delivery_fees << DeliveryFee.new(range['min_price'].. range['max_price'] || Float::INFINITY, delivery_fee_hash['price'])
		end

		data_hash['discounts'].each do |discounts_hash|
			discounts << Discount.new(discounts_hash['article_id'], discounts_hash['type'], discounts_hash['value'])
		end

		Ecommerce.new(articles, carts, delivery_fees, discounts)
	end

	# write the data on the file .json
	def write_data
		data = {carts: []}

		carts.each do |cart|
			data[:carts] << {id: cart.id, total: cart.total(@articles, @delivery_fees, @discounts)}
		end

		File.open("output_res.json","w") do |f|
		  f.write(JSON.pretty_generate(data))
		end
	end

end

ecommerce = Ecommerce.load_data
ecommerce.write_data