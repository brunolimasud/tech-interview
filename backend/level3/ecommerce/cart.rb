require './item'
require 'byebug'

class Cart
  attr_accessor :items
  attr_reader :id

  def initialize id, items
  	@id = id
  	@items = []

  	items.each do |item_hash|
  		@items << Item.new(item_hash[:article_id] || item_hash['article_id'], item_hash[:quantity] || item_hash['quantity'])
  	end
  end

  #calculate cart's total value with discounts and delivery fees
  def total articles, delivery_fees=[], discounts=[]
  	total = 0

  	items.each do |item|
  		article = articles.select {|article| article.id == item.article_id }.first
      discount = discounts.select {|discount| discount.article_id == item.article_id}.first

      price = discount.nil? ? article.price : discount.calculate_price(article.price)
      total += price*item.quantity
  	end

    delivery_fee = delivery_fees.select {|delivery_fee| delivery_fee.minmax.include?(total) }.last
    delivery_fee_value = delivery_fee.nil? ? 0 : delivery_fee.price

  	total + delivery_fee_value
  end
end