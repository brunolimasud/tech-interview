require 'json'
require 'byebug'

require './article'
require './cart'

class Ecommerce
  attr_accessor :articles, :carts

  def initialize articles, carts
  	@articles = articles
  	@carts = carts
  end

  def self.load_data
  	data_file = File.read('data.json')
		data_hash = JSON.parse(data_file)

		articles = []
		carts = []

		data_hash['articles'].each do |article_hash|
			articles << Article.new(article_hash['id'], article_hash['name'], article_hash['price'])
		end

		data_hash['carts'].each do |cart_hash|
			carts << Cart.new(cart_hash['id'], cart_hash['items'])
		end

		Ecommerce.new(articles, carts)
	end

	def write_data
		data = {carts: []}

		carts.each do |cart|
			data[:carts] << {id: cart.id, total: cart.total(articles)}
		end

		File.open("output_res.json","w") do |f|
		  f.write(JSON.pretty_generate(data))
		end
	end

end

ecommerce = Ecommerce.load_data
ecommerce.write_data