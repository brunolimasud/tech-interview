require './item'

class Cart
  attr_accessor :items
  attr_reader :id

  def initialize id, items
  	@id = id
  	@items = []

  	items.each do |item_hash|
  		@items << Item.new(item_hash['article_id'], item_hash['quantity'])
  	end
  end

  def total articles
  	total = 0

  	items.each do |item|
  		article = articles.select {|article| article.id == item.article_id }.first
  		total += article.price*item.quantity
  	end

  	total
  end
end