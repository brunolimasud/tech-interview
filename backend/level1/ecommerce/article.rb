class Article
  attr_accessor :name, :price
  attr_reader :id

  def initialize id, name, price
  	@id = id
  	@name = name
  	@price = price
  end
end