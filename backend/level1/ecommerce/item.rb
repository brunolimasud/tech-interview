class Item
  attr_accessor :article_id, :quantity

  def initialize article_id, quantity
  	@article_id = article_id 
  	@quantity = quantity
  end
end