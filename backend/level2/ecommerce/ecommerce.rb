require 'json'
require 'byebug'

require './article'
require './cart'
require './delivery_fee'

class Ecommerce
  attr_accessor :articles, :carts

  def initialize articles, carts, delivery_fees
  	@articles = articles
  	@carts = carts
  	@delivery_fees = delivery_fees
  end

  def self.load_data
  	data_file = File.read('data.json')
		data_hash = JSON.parse(data_file)

		articles = []
		carts = []
		delivery_fees = []

		data_hash['articles'].each do |article_hash|
			articles << Article.new(article_hash['id'], article_hash['name'], article_hash['price'])
		end

		data_hash['carts'].each do |cart_hash|
			carts << Cart.new(cart_hash['id'], cart_hash['items'])
		end

		data_hash['delivery_fees'].each do |delivery_fee_hash|
			range = delivery_fee_hash['eligible_transaction_volume']

			delivery_fees << DeliveryFee.new(range['min_price'].. range['max_price'] || Float::INFINITY, delivery_fee_hash['price'])
		end

		Ecommerce.new(articles, carts, delivery_fees)
	end

	def write_data
		data = {carts: []}

		carts.each do |cart|
			data[:carts] << {id: cart.id, total: cart.total(@articles, @delivery_fees)}
		end

		File.open("output_res.json","w") do |f|
		  f.write(JSON.pretty_generate(data))
		end
	end

end

ecommerce = Ecommerce.load_data
ecommerce.write_data