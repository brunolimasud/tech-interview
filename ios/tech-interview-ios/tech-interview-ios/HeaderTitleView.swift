//
//  HeaderTitleView.swift
//  tech-interview-ios
//
//  Created by Bruno de Lima Marques on 02/02/17.
//  Copyright © 2017 Bruno de Lima Marques. All rights reserved.
//

import UIKit

class HeaderTitleUIView: UIView {

    @IBOutlet weak var categoryTitle: UILabel!
    
}
