//
//  Title.swift
//  tech-interview-ios
//
//  Created by Bruno de Lima Marques on 02/02/17.
//  Copyright © 2017 Bruno de Lima Marques. All rights reserved.
//

import UIKit
import Alamofire
import EVReflection

class Title: EVObject {
    
    var category: String?
    var items: [Item] = [Item]()
    
    
    //get the title's indexPath that item belongs
    //is needed when is showing favorite items
    class func indexPathFavoriteTitleSelected(titles: [Title], item: Item) -> IndexPath? {
        
        for i in 0..<titles.count {
            for j in 0..<titles[i].items.count {
                if titles[i].items[j] == item { return IndexPath(row: j, section: i) }
            }
        }
        
        return nil;
    }

}
