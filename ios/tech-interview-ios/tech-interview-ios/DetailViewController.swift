//
//  DetailViewController.swift
//  tech-interview-ios
//
//  Created by Bruno de Lima Marques on 02/02/17.
//  Copyright © 2017 Bruno de Lima Marques. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var titleSelected : Title?
    var indexSelected: Int = 0
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleItem: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        scheduleTimer()
        setColorStar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    //config views with item attributes
    func configureViews() {
        if let titleSelected = titleSelected {
            let item = titleSelected.items[indexSelected]
            subTitle.text = titleSelected.category
            desc.text = item.desc
            
            if let galeryImages = item.galeryImages {
                imageView.image = galeryImages[item.imageShowingIndex];
                titleItem.text = item.title?.uppercased()
            }
        }
    }
    
    //schedule change image animation each 3 seconds
    func scheduleTimer() {
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.loadNextImage), userInfo: nil, repeats: true)
    }

    @IBAction func backHomeButton(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func favoriteItem(_ sender: UIButton) {
        if let titleSelected = titleSelected {
            let item = titleSelected.items[indexSelected]
            item.favorite = !item.favorite
            
            setColorStar()
        }
        
    }
    
    //set favorite icon's color with yellow or white
    func setColorStar() {
        if let titleSelected = titleSelected {
            let item = titleSelected.items[indexSelected]
            
            let origImage = UIImage(named: "ic_star")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            favoriteButton.setImage(tintedImage, for: .normal)
            favoriteButton.tintColor = item.favorite ? .yellow : .white
        }
    }
    
    //load next image animated
    func loadNextImage() {
        if let titleSelected = titleSelected {
            let item = titleSelected.items[indexSelected]
            
            item.nextImage(completionHandler: { (success, image) in
                if success, let image = image {
                    
                    UIView.transition(with: self.imageView, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.imageView.image = image
                    }, completion: nil)
                }
            })
        }
    }
    
}
