//
//  Item.swift
//  tech-interview-ios
//
//  Created by Bruno de Lima Marques on 02/02/17.
//  Copyright © 2017 Bruno de Lima Marques. All rights reserved.
//

import UIKit
import EVReflection

class Item: EVObject {

    var title: String?
    var desc: String?
    var galery: [String] = [String]()
    var galeryImages : [UIImage?]?
    var imageShowingIndex = 0
    var favorite: Bool = false
    
    override public func propertyMapping() -> [(String?, String?)] {
        return [("description","desc"), ("desc","description")]
    }
    
    //get next image
    func nextImage(completionHandler: @escaping (_ success: Bool, _ image: UIImage?) -> ()) {
        imageShowingIndex+=1;
        
        if imageShowingIndex >= galery.count {
            imageShowingIndex = 0
        }
        
        requestImage(completionHandler: completionHandler)
    }
    
    //get previous image
    func prevImage(completionHandler: @escaping (_ success: Bool, _ image: UIImage?) -> ()) {
        imageShowingIndex-=1;
        
        if imageShowingIndex < 0 {
            imageShowingIndex = galery.count-1
        }
        
        requestImage(completionHandler: completionHandler)
    }
    
    
    //download item image
    func requestImage(completionHandler: @escaping (_ success: Bool, _ image: UIImage?) -> ()) {
        
        if let galeryImages = galeryImages, let image = galeryImages[imageShowingIndex] {
            completionHandler(true, image)
            return
        }
        
        RequestData.downloadImages(url: galery[imageShowingIndex]) { (success, image) in
            
            if success, let image = image {
                if self.galeryImages == nil || self.galeryImages?.count == 0 {
                    self.galeryImages = Array(repeating: nil, count: self.galery.count)
                }
                
                self.galeryImages![self.imageShowingIndex] = image
                completionHandler(true, image)
            }
            else {
                completionHandler(false, nil)
            }
            
        }
    }
}
