//
//  RequestData.swift
//  tech-interview-ios
//
//  Created by Bruno de Lima Marques on 02/02/17.
//  Copyright © 2017 Bruno de Lima Marques. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireJsonToObjects
import AlamofireImage

class RequestData: NSObject {

    static let urlJSON = "https://cdn.joyjet.com/tech-interview/mobile-test-one.json"
    
    class func loadTitles(completionHandler: @escaping (_ success: Bool, _ titles: [Title]?) -> ()) {
        Alamofire.request(urlJSON).responseArray(completionHandler: { (response: DataResponse<[Title]>) in
            if let resultArray = response.result.value {
                var titles = [Title]()
                
                for i in 0..<resultArray.count {
                    titles.append(resultArray[i])
                }
                
                completionHandler(true, titles)
            }
            else {
                completionHandler(false, nil)
            }
            
        })
        
    }
    
    class func downloadImages(url: String?, completionHandler: @escaping (_ success: Bool, _ image: UIImage?) -> ()) {
        guard let url = url else {
            completionHandler(false, nil)
            return
        }
        
        Alamofire.request(url).responseImage { (response) in
            if let image = response.result.value {
                completionHandler(true, image)
            }
            else {
                completionHandler(false, nil)
            }
        }
    }

}
