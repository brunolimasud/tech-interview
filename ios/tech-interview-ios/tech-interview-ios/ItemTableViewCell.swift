//
//  ItemTableViewCell.swift
//  tech-interview-ios
//
//  Created by Bruno de Lima Marques on 02/02/17.
//  Copyright © 2017 Bruno de Lima Marques. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var titleItem: UILabel!
    @IBOutlet weak var descItem: UILabel!
    
    var item: Item?
    
    @IBAction func showPrevImage(_ sender: UIButton) {
        if let item = item {
            item.prevImage(completionHandler: { (success, image) in
                if success, let image = image {
                    self.changeImageAnimated(image: image)
                }
            })
        }
    }
    
    @IBAction func showNextImage(_ sender: UIButton) {
        if let item = item {
            item.nextImage(completionHandler: { (success, image) in
                if success, let image = image {
                    self.changeImageAnimated(image: image)
                }
            })
        }
    }
    
    func changeImageAnimated(image: UIImage) {
        UIView.transition(with: backgroundImage, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.backgroundImage.image = image
        }, completion: nil)
    }

}
