//
//  MenuViewController.swift
//  tech-interview-ios
//
//  Created by Bruno de Lima Marques on 03/02/17.
//  Copyright © 2017 Bruno de Lima Marques. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class MenuViewController: UIViewController {

    @IBOutlet weak var homeOptionView: UIView!
    @IBOutlet weak var favoritesOptionView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureGestures()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //adjust width menu controller
        SlideMenuOptions.rightViewWidth=320
    }
    
    //configure tap gestures in options views
    func configureGestures() {
        let homeTap = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.showHome(sender:)))
        homeOptionView.addGestureRecognizer(homeTap)
        
        let favoritesTap = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.showFavorites(sender:)))
        favoritesOptionView.addGestureRecognizer(favoritesTap)
        
    }
    
    func showHome(sender: UITapGestureRecognizer) {
        changeViewController(name: "Digital Space", showFavorites: false)
    }

    func showFavorites(sender: UITapGestureRecognizer) {
        changeViewController(name: "Favorites", showFavorites: true)
    }
    
    func changeViewController(name: String, showFavorites: Bool) {
        if let navigationController = self.slideMenuController()?.mainViewController as? UINavigationController, let homeViewController = navigationController.viewControllers[0] as? HomeViewController {
    
            //set the title navigation bar and change the content
            homeViewController.changeContent(title: name, showFavorites: showFavorites)
        }
        
        self.slideMenuController()?.closeRight()
    }

}
