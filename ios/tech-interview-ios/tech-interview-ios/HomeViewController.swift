//
//  ViewController.swift
//  tech-interview-ios
//
//  Created by Bruno de Lima Marques on 02/02/17.
//  Copyright © 2017 Bruno de Lima Marques. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class HomeViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var showingFavotires = false;
    var titles = [Title]()
    
    
    var favoriteItems: [Item] {
        get {
            var items = titles.map { $0.items }.flatMap({$0})
            items = items.filter({ $0.favorite })
            
            return items
        }
    }
    
    
    //MARK: -Override methods UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        
        if titles.count == 0 {
            loadTitles()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    @IBAction func showMenu(_ sender: UIBarButtonItem) {
        self.slideMenuController()?.openRight()
    }
    
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets.zero;
    }
    
    
    //load json file
    func loadTitles() {
        RequestData.loadTitles { (success, titles) in
            if success, let titles = titles {
                self.titles = titles
                self.tableView.reloadData()
            }
        }
    }
    
    
    //change the controller to show favorite items
    func changeContent(title: String, showFavorites: Bool) {
        showingFavotires = showFavorites
        self.navigationItem.title = title
        
        self.tableView.reloadData()
    }
    
    
    //MARK: - Table View Data Source Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var quantity = 0
        
        if titles.count > 0 {
            quantity = titles[section].items.count
        }
        
        if showingFavotires {
            quantity = favoriteItems.count
        }
        
        return quantity
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var quantity = titles.count
        
        if showingFavotires {
            quantity = 1
        }
        
        return quantity
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        if let cell = cell as? ItemTableViewCell {
            let item = showingFavotires ? favoriteItems[indexPath.row] : titles[indexPath.section].items[indexPath.row]
            
            cell.item = item
            cell.titleItem.text = item.title
            
            //get the string until the first dot
            if let range = item.desc?.range(of: ".") {
                cell.descItem.text = item.desc?.substring(to: range.lowerBound)
            }
            
            //clear the previous reuseable cell's image
            cell.backgroundImage.image = nil
            
            item.requestImage(completionHandler: { (success, image) in
                if success, let image = image {
                    
                    UIView.transition(with: cell.backgroundImage, duration: 0.75, options: .transitionCrossDissolve, animations: {
                        cell.backgroundImage.image = image
                    }, completion: nil)
                    
                    cell.backgroundImage.image = image
                }
            })
            
        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return showingFavotires ? 0 : 34
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if showingFavotires { return nil }
        
        let title = titles[section]
        
        let nibView = Bundle.main.loadNibNamed("HeaderTitleView", owner: self, options: nil)?[0]
        if let view = nibView as? HeaderTitleUIView {
            view.categoryTitle.text = title.category
        }
        
        return nibView as? UIView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRect.zero)
    }
    
    //MARK: - Table View Delegate Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: false)
        
        if showingFavotires {
            let item = favoriteItems[indexPath.row]
            
            if let indexPath = Title.indexPathFavoriteTitleSelected(titles: titles, item: item) {
                performSegue(withIdentifier: "showDetail", sender: indexPath)
                return
            }
        }
        
        performSegue(withIdentifier: "showDetail", sender: indexPath)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail", let destinationContrller = segue.destination as? DetailViewController, let indexPath = sender as? IndexPath {
            destinationContrller.titleSelected = titles[indexPath.section]
            destinationContrller.indexSelected = indexPath.row
        }
    }
}

